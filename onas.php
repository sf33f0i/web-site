<?php 
require "db.php";
require "newmsg.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="./script\openmenu.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&family=Roboto&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dieta</title>
</head>
<body>
       
    <div class="basecon2">
    <?php require "header.php"; ?>
        <div class="bigblockonas">
        <div class="spaсe123"></div>
        <div class="flexblock1">
            <div class="nutri2">
                <div class="redBlock"><span class="headertext1">Нутрициолог</span></div>  
                <div class="neredBlock"> <span class="headertext2">— это специалист, который изучает науку о питании, проводит обучающие программы или персональные консультации по питанию.</span></div>
            </div>
        </div>
    </div>
    </div>
    <div class="containeronas">
        <div class="stryktura">
        <div class="onascon1">
            <div class="onascon23">
            <img class="imgstryktura" src="images\frukti.png" alt="" width="563px" height="377px">
            </div>

        </div>
        <div class="onascon2">
            <div class="onascon231">
                <span><strong> В компетенцию нутрициолога входят: </strong><br><br>

                    - изучение влияния пищи на здоровье человека, получение знаний о физиологии и биохимических процессах пищеварения, макро- и микронутриентах, витаминном-минеральном составе продуктов питания<br><br>
                    
                    - перевод и информирование людей об актуальных рекомендациях по питанию из открытых источников официальных организаций в области общественного здравоохранения (ВОЗ, NHS, USDA, и др.)<br><br>
                    
                    - составление сбалансированного по макро- и микронутриентам рациона питания для здоровых людей<br><br>
                    
                    - подбор рациона для спортсменов для улучшения результатов на тренировках и соревнованиях<br><br>
                    
                    - изучение составов продуктов, помощь в выборе наиболее качественных<br><br>
                    
                    - изучение способов приготовления пищи и их влияние на сохранение полезных свойств в блюдах<br><br>
                    
                    - помощь в улучшении привычек (обучение).</span>
            </div>
        </div>
        </div>
    </div>
    <div class="containeronas111">
        <div class="stryktura2">
        <div class="onascon1">
            <div class="onascon23">
            <img class="imgstryktura" src="images\frukti2.jpg" alt="" width="563px" height="377px">
            </div>

        </div>
        <div class="onascon2">
            <div class="onascon23">
                <span>Не секрет, что современные пищевые продукты в обилии содержат всевозможные консерванты, стабилизаторы, усилители вкуса и т. д. В итоге наша пища теряет большую часть питательности и полезности. Чтобы наверстать дефицит важных и необходимых веществ и не допустить появления сбоев в организме, и существует профессия нутрициолога. <br><br>

                    Нутрициолог знает обо всех закономерностях воздействия пищевых ингредиентов на организм и между собой, а также о влиянии принципов употребления пищи на человеческое здоровье.
                    Существует два средства для поддержания здоровья и красоты: первое - активные физические упражнения, второе - рациональное, сбалансированное питание. <br><br>
                    
                    Каждый человек мечтает найти магический набор продуктов, потребление которых позволило бы иметь отличную фигуру, обладать неиссякаемой энергией и оставаться молодым и здоровым.<br><br>
                    
                    Грамотно подобранное питание в дополнении к физическим нагрузкам позволит достигнуть оптимального результата, в то время как неполноценное питание может не только свести на нет все Ваши усилия, но и навредить Вашему здоровью.</span>
            </div>
        </div>
        </div>
    </div>
    <footer class="footer">
        <div class="footer21">
            <div class="brawlstars">
            <div class="footerAdress">
                <span class="Adress">Адрес</span><br>
                <span class="Adress2">г. Ханты-Мансийск, ул. Студенческая 5</span><br><br>
                <span class="Adress">Контакты</span><br>
                <span class="Adress2">+7 (908)  906 82-57</span>
            </div>
            <div class="footerNav">
                <span class="Adress">Навигация</span><br>
                <nav class="footerNav2">
                    <a class="footerHref" href="./index.php">Главная</a>
                    <a class="footerHref" href="./galery.php">Галерея</a>
                    <a class="footerHref" href="./price.php">Цены</a>
                    <a class="footerHref" href="./kontakti.php">Контакты</a>
                </nav>
            </div>
        </div>
            <div class="footerImg">
                <span class="Adress">Мы в социальных сетях:</span>
                <div class="footerImg2">
                    <img class="imginsta" src="./images\insta2.png" alt="" width="37.42px" height="37px">
                    <img class="imgFace" src="./images\face.png" alt="" width="39.44px" height="39px">
                    <img class="imgtwit" src="./images\twit.png" alt="" width="40.45px" height="32.5px">
                </div>
                <div class="lineFooter"></div>
            </div>
        </div>
    </footer>
</body>
</html>