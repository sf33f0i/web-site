<footer class="footer">
    <div class="footer21">
        <div class="brawlstars">
        <div class="footerAdress">
            <span class="Adress">Адрес</span><br>
            <span class="Adress2">г. Ханты-Мансийск, ул. Студенческая 5</span><br><br>
            <span class="Adress">Контакты</span><br>
            <span class="Adress2">+7 (908)  906 82-57</span>
        </div>
        <div class="footerNav">
            <span class="Adress">Навигация</span><br>
            <nav class="footerNav2">
                <a class="footerHref" href="./index.php">Главная</a>
                <a class="footerHref" href="./galery.php">Галерея</a>
                <a class="footerHref" href="./price.php">Цены</a>
                <a class="footerHref" href="./kontakti.php">Контакты</a>
            </nav>
        </div>
        </div>
        <div class="footerImg">
            <span class="Adress">Мы в социальных сетях:</span>
            <div class="footerImg2">
                <img class="imginsta" src="./images\insta2.png" alt="" width="37.42px" height="37px">
                <img class="imgFace" src="./images\face.png" alt="" width="39.44px" height="39px">
                <img class="imgtwit" src="./images\twit.png" alt="" width="40.45px" height="32.5px">
            </div>
            <div class="lineFooter"></div>
        </div>
    </div>
</footer>