<?php 
require 'db.php';
$data=$_POST;
$useraimbow=$_SESSION['logged_user']->id;
$roleuser=$_SESSION['logged_user']->role;
$errors=array();
if(isset($data['accept'])){
    $accept= R::load('applications', $_GET['id']);
    $accept -> status = 1;
    R::store($accept);
}
if(isset($data['deleteapp'])){
    $delete=R::load('applications', $_GET['id']);
    $delete-> status = 2;
    R::store($delete);
    header('Location: ' . $_SERVER['HTTP_REFERER']);
}

if(isset($data['trash'])){
    $delete=R::load('applications', $_GET['id']);
    R::trash($delete);
    header('Location: ' . $_SERVER['HTTP_REFERER']);
}
if(isset($data['Save'])){
    if($data['aim']==''){
        $errors[]='Введите цель';
    }
    if($data['date']==''){
        $errors[]='Введите дату окончания!';
    }
    if($data['date']<date("Y-m-d")){
        $errors[]='Выбрана не та дата';
    }
    if(empty($errors)){
        $useraim=R::dispense('usersaims');
        $useraim->aim = $data['aim'];
        $useraim->iduser= $useraimbow;
        $useraim->date = $data['date'];
        R::store($useraim);
        $smsg='Цель успешно создана';
    }
    else{
        $fsmsg= array_shift($errors);
    }
}
$foradmin=R::findALL('applications');
$foradmin2=R::findALL('users');
$query= R::findLike('usersaims' , array( 'iduser'=> array($useraimbow)));

if (isset($_POST['edit-submit'])) {
    $aim = R::load('usersaims', $_GET['id']);
    $aim -> aim = $_POST['edit_aim'];
    $aim -> date = $_POST['edit_date'];
    R::store($aim);
    header('Location: ' . $_SERVER['HTTP_REFERER']);
}
if (isset($_POST['delete_submit'])) {
    $aimtrash=R::load('usersaims', $_GET['id']);
    R::trash($aimtrash);

    header('Location: ' . $_SERVER['HTTP_REFERER']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="./script\openmenu.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&family=Roboto&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style228.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    </head>
<body>
<div class="basecon">
<?php require "header.php"; ?>
    <?php if($roleuser==true) :?>
        <div class="widthform">
        <div class="formaclassa">
    <div class="dino">
    <table class="table table-striped table-hover mt-2">
					<thead class="table-dark">
						<tr>
							<th>ID</th>
							<th>ФИО</th>
                            <th>Телефон</th>
                            <th>Проблема</th>
                            <th>Статус</th>
                            <th> Принять/Отклонить</th>

						</tr>
					</thead>
					<tbody>
					<?php foreach ($foradmin as $value) { ?>
						<tr>
							<td><?=$value['id'] ?></td>
							<td><?=$value['fio'] ?></td>
                            <td><?=$value['phone'] ?></td>
                            <td><?=$value['problem'] ?></td>
                            <td><? 
                            if($value['status']==1){echo "Принято";}
                            if($value['status']==2){echo "Отклонено";}
                            if($value['status']==0){echo "На рассмотрении";}
                            if($value['status']==4){echo "Пользорватель готов";}
                            if($value['status']==5){echo "Пользователь оповещен об отказе";}
                            ?></td>
                            <td>
                                <form action="?id=<?=$value['id'] ?>" method='POST'>
								<button type="submit" name="accept" class="btn btn-success btn-sm"><i class="fa fa-check" aria-hidden="true"></i></button>
								<button type="submit"  name="deleteapp" class="btn btn-danger btn-sm"><i class="fa fa-times" aria-hidden="true"></i></button>
                                <button type="submit"  name="trash" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button>
								<?php require 'modal.php'; ?>
                                </form>
							</td>
						</tr> <?php } ?>
					</tbody>
				</table>
                <table class="table table-striped table-hover mt-2">
					<thead class="table-dark">
						<tr>
							<th>ID</th>
							<th>ФИО</th>
                            <th>Почта</th>
                            <th>Роль</th>

   

						</tr>
					</thead>
					<tbody>
					<?php foreach ($foradmin2 as $value) { ?>
						<tr>
							<td><?=$value['id'] ?></td>
							<td><?=$value['login'] ?></td>
                            <td><?=$value['email'] ?></td>
                            <td><?=$value['role'] ?></td>
     
						</tr> <?php } ?>
					</tbody>
				</table>
    </div>
    </div>
    <?php  else: ?>
    <div class="widthform">
    <div class="formaclassa">
    <div class="dino">
    <table class="table table-striped table-hover mt-2">
					<thead class="table-white">
						<tr>
							<th>Цель</th>
							<th>Дата</th>
                            <th>Удалить/Изменить</th>

						</tr>
					</thead>
					<tbody>
					<?php foreach ($query as $value) { ?>
						<tr>
							<td><?=$value['aim'] ?></td>
							<td><?=$value['date'] ?></td>
							<td>
								<a href="?edit=<?=$value['id'] ?>" class="btn btn-success btn-sm" data-toggle="modal" data-target="#editModal<?=$value['id'] ?>"><i class="fa fa-edit"></i></a> 
								<a href="?delete=<?=$value['id'] ?>" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal<?=$value['id'] ?>"><i class="fa fa-trash"></i></a>
								<?php require 'modal.php'; ?>
							</td>
						</tr> <?php } ?>
					</tbody>
				</table>
    </div>
    <?php endif; ?>
    <?php if($roleuser==false) :?>
    <div class="form-class">
        <form class="formlad" method="POST">
        <?php if(isset($smsg)){?><div class="alert alert-success" role="alert"> <?php echo $smsg; ?> </div><?php }?>
        <?php if(isset($fsmsg)){?><div class="alert alert-danger" role="alert"> <?php echo $fsmsg; ?> </div><?php }?>
        <input class="form-control" type="text" name="aim" placeholder="Цель">
        <input class="form-control" type="date" name="date" placeholder="Срок выполнения">
        <button type="submit" name="Save" class="btn btn-primary">Сохранить</button>
        </form>
    </div>
    </div>
   </div>
   </div>
   <?php endif; ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>