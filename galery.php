<?php 
require "db.php";
require "newmsg.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <script src="https://snipp.ru/cdn/jquery/2.1.1/jquery.min.js"></script>
    <script>  
$(function(){
	$('#scroll_bottom').click(function(){
		$('html, body').animate({scrollTop: 1100}, 600);
		return false;
	});
});
    </script>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&family=Roboto&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
    <title>Dieta</title>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script src="./script\openmenu.js"></script>
</head>
<body>
    <div class="basecon">
    <?php require "header.php"; ?>
    <div class="containergaley1">
        <span class="galerytext1">Галерея</span>
        <span class="galerytext2">Последние три наших клиента</span>
    </div>
    <div class="containergaley2">
        <div class="imggalegy1">
            <a data-fancybox="images" href="images\4.jpg"  data-caption="Клиент 1"><img class="img1" src="images\4.jpg" alt="" width="226px" height="218px"></a>
            <a data-fancybox="images" href="images\2.jpg"  data-caption="Клиент 2"><img class="img1" src="images\2.jpg" alt="" width="226px" height="218px"></a>
            <a data-fancybox="images" href="images\3.jpg"  data-caption="Клиент 3"><img class="img1" src="images\3.jpg" alt="" width="226px" height="218px"></a>
           
        </div>
        <a class="scrollbottom" href="#" id="scroll_bottom"><img src="images\scrollbottom.png" alt="" width="170px" height="100px"></a>
    </div>
    <div class="containergaley3">
        <div class="galeryimg3">
            <a data-fancybox="images" href="images\6.jpg"  data-caption="Клиент 1"><img class="img1" src="images\6.jpg" alt="" width="350PX" height="341px"></a>
            <a data-fancybox="images" href="images\7.jpg"  data-caption="Клиент 2"><img class="img1" src="images\7.jpg" alt="" width="350PX" height="341px"></a>
            <a data-fancybox="images" href="images\8.jpg"  data-caption="Клиент 3"><img class="img1" src="images\8.jpg" alt="" width="350px" height="341px"></a>
        </div>
        <div class="galeryimg4">
            <a data-fancybox="images" href="images\5.webp"  data-caption="Клиент 4"><img class="img1" src="images\5.webp" alt="" width="350px" height="341px"></a>
            <a data-fancybox="images" href="images\9.jpg"  data-caption="Клиент 5"><img class="img1" src="images\9.jpg" alt="" width="350px" height="341px"></a>
            <a data-fancybox="images" href="https://avatars.mds.yandex.net/get-zen_doc/175604/pub_5cecebe8563a6c00b2698509_5ced0dee77884b00afa9a53c/scale_1200"  data-caption="Клиент 6"><img class="img1" src="https://avatars.mds.yandex.net/get-zen_doc/175604/pub_5cecebe8563a6c00b2698509_5ced0dee77884b00afa9a53c/scale_1200" alt="" width="350px" height="341px"></a>
        </div>
    </div>
    <?php require "footer.php";?>
</body>
</html>