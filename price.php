<?php 
require "db.php";
require "newmsg.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="./script\openmenu.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&family=Roboto&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dieta</title>
</head>
<body>
       
    <div class="basecon2">
    <?php require "header.php"; ?>
        <div class="basecon3">
            <div class="b123321">
            <span class="textheadprice1">Услуги и цены нутрициолога</span>
            <span class="textheadprice2">Мы индивидуально подходим к каждому клиенту</span>
            </div>
        </div>
    </div>
    <div class="price">
        <div class="price01">
            <img class="priceIMG1" src="./images\tort.png" alt="">
            <div class="dljaFlexa">
            <div class="textprice1">
                <span class="textprice11"> 
                    Первичная консультация - 1000 рублей
                </span>
                <span class="textprice12">
                    Обсудим с Вами проблемы и пожелания.<br><br>

                    Поймем природу Ваших проблем и определим первичные этапы оздоровления.
                </span>
            </div>
            <div class="rectanglezajavka">
                <span class="rectangle123">
                    Забронировать время
                </span>
            </div>
        </div>
        </div>
    </div>
    <div class="price">
        <div class="price012">
            <div class="dljaFlexa2">
            <div class="textprice2">
                <span class="textprice21"> 
                    Консультирование с рекомендациями
- 5000 рублей.
                </span>
                <span class="textprice22">
                    Индивидуальное сопровождение Вашего оздоровления. <br><br>

                    Разработка плана питания и процедур.<br><br>
                    
                    Контроль Вашего состояния.<br><br>
                    
                    Ведение и учет дневника.
                </span>
            </div>
            <div class="rectanglezajavka">
                <span class="rectangle123">
                    Забронировать время
                </span>
            </div>
        </div>
        <img class="priceIMG2" src="./images\chel.png" alt="" width="613px" height="462px">
        </div>
    </div>
    <div class="price">
        <div class="price01">
            <img class="priceIMG3" src="./images\tarelka.png" alt="" width="613px" height="462px">
            <div class="dljaFlexa">
            <div class="textprice1">
                <span class="textprice11"> 
                    Детокс СПА терапия - <br>1500 рублей за сеанс 50 минут.
                </span>
                <span class="textprice12">
                    Ионное очищение организма – это быстрый и безболезненный способ для предупреждения или помощи при заболеваниях, которым подвержено современное общество.
                </span>
            </div>
            <div class="rectanglezajavka">
                <span class="rectangle123">
                    Забронировать время
                </span>
            </div>
        </div>
        </div>
    </div>
    <div class="price4">
        <div class="price013">
            <div class="dljaFlexa3">
            <div class="textprice3">
                <span class="textprice21"> 
                    Су джок Терапия - 1500 рублей за сеанс.
                </span>
                <span class="textprice22">
                    Су-джок-терапия – это оригинальный способ проработки рефлекторных точек кистей и стоп, позволяющий восстановить функции организма, предупредить развитие серьезных заболеваний и оказать скорую помощь человеку, чье здоровье находится под угрозой. Наши современники всё чаще и чаще обращаются к этой восточной технике, пытаясь избавиться от хронических недугов, боли, депрессии, неврозов или банальной усталости
                </span>
            </div>
            <div class="rectanglezajavka">
                <span class="rectangle123">
                    Забронировать время
                </span>
            </div>
        </div>
        <img class="priceIMG4" src="./images\kaktus.png" alt="" width="613px" height="462px">
        </div>
    </div>
    <footer class="footer">
        <div class="footer21">
            <div class="brawlstars">
            <div class="footerAdress">
                <span class="Adress">Адрес</span><br>
                <span class="Adress2">г. Ханты-Мансийск, ул. Студенческая 5</span><br><br>
                <span class="Adress">Контакты</span><br>
                <span class="Adress2">+7 (908)  906 82-57</span>
            </div>
            <div class="footerNav">
                <span class="Adress">Навигация</span><br>
                <nav class="footerNav2">
                    <a class="footerHref" href="./index.php">Главная</a>
                    <a class="footerHref" href="#">Галерея</a>
                    <a class="footerHref" href="#">Цены</a>
                    <a class="footerHref" href="./kontakti.php">Контакты</a>
                </nav>
            </div>
        </div>
            <div class="footerImg">
                <span class="Adress">Мы в социальных сетях:</span>
                <div class="footerImg2">
                    <img class="imginsta" src="./images\insta2.png" alt="" width="37.42px" height="37px">
                    <img class="imgFace" src="./images\face.png" alt="" width="39.44px" height="39px">
                    <img class="imgtwit" src="./images\twit.png" alt="" width="40.45px" height="32.5px">
                </div>
                <div class="lineFooter"></div>
            </div>
        </div>
    </footer>
</body>
</html>