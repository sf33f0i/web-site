<?php 
require "db.php";
require "newmsg.php";
$iduserr=$_SESSION['logged_user']->id;
$data=$_POST;
$errors=array();
if(isset($_POST['application'])){
 if(trim($data['fioinput']=='')){
     $errors[]='Введите ФИО';
 }
 if(trim($data['phone']=='')){
     $errors[]='Введите номер телефона';
 }
 if(trim($data['problem']=='')){
     $errors[]= 'Опишите вашу проблему!';
 }
 if(R::count('applications', "idus= ?",array($iduserr))>0)
 {
     $errors[]="Вы уже отправляли заявку ранее!";
 }
 if(empty($errors)){
    $application=R::dispense('applications');
    $application-> fio = $data['fioinput'];
    $application-> phone = $data['phone'];
    $application-> idus = $iduserr;
    $application-> problem = $data['problem'];
    $application-> status = 0;
    R::store($application);
    $smsg='Заявка отправлена на рассмотрение!';
 }
 else{
     $fsmsg=array_shift($errors);
 }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&family=Roboto&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&family=Roboto&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="./script\openmenu.js"></script>
    <title>Dieta</title>
</head>
<body>
       
    <div class="basecon">
    <?php require "header.php"; ?>
    <div class="container4">
    <div class="forma">
        <div class="flexzajavka">
            <span class="poster">Оставить заявку</span>
            <?php if(isset($smsg)){?><div class="alert alert-success" role="alert"> <?php echo $smsg; ?> </div><?php }?>
            <?php if(isset($fsmsg)){?><div class="alert alert-danger" role="alert"> <?php echo $fsmsg; ?> </div><?php }?>
            <form class="formforflex" method="POST">
            
                <p class="textZAJAVKI">Ф.И.О<br>
                    <input type="text" name="fioinput" class="input1"value="<?php echo @$data['fioinput']; ?>">
                </p>
                <p class="textZAJAVKI2"> Номер телефона(без +7 или 8)<br><input type="tel" id="phone" name="phone" class="input2"
                 pattern="[0-9]{3}[0-9]{3}[0-9]{4}" value="<?php echo @$data['phone']; ?>">
                </p>
                <p class="textZAJAVKI3"> Опишите вашу проблему<br>
                    <textarea type="text" name="problem" class="input3" width="338px"><?php echo @$data['problem']; ?></textarea>
                </p>
        </div>
                <div class="spaceforbutton">           
                <div class="formoblast"> 
                    <input name="application" class="buttonius" type="submit" value="Отправить">
                </div>
                </div>
            </nav>
            </div>
        </form>        
    </div>
</div>    
    </div>
</div>
<?php require "footer.php";?>
</body>
</html>