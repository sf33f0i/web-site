<?php 
require "db.php";
require "newmsg.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
     <script src="./script\openmenu.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&family=Roboto&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dieta</title>
</head>
<body>
       
    <div class="basecon2">
    <?php require "header.php"; ?>
        <div class="KONTAKT">
            <span>Контакты</span>
        </div> 
    </div>
    <div class="MediumKontakt">
        <div class="obertka">
            <div class="kontakttext1">
                <span>Контактная информация</span>
            </div>
            <div class="kontakttext2">
                <span>Мы рады общаться и встретиться с вами</span>
            </div>
            <div class="kontakttext3">
                <span>Медицинское издание «ProFiD» существует с января 2018 года. В 2020 году мы открыли интернет представительство, которое находится по адресу ProFiD.ru и с 2020 г мы работаем только во всемирной сети.

                    <br><br>Подробнее об истории развития и миссии компании вы можете прочитать на странице <a class="ssilkaonas" href="\onas.php">"О нас"</a></span>
            </div>
        </div>
    </div>
    <div class="container3">
        <div class="mapandtext">
                <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A85bd006b8917f1d2a098aa1d947e13d3248a49d95d43837c00525c2f88c982e1&amp;width=100%25&amp;height=100%25&amp;lang=ru_RU&amp;scroll=true"></script> 
                <div class="space"></div> 
        <div class="anime">  
            <div class="text33">
                <h1> Адрес:
                    <br><br>г. Ханты-Мансийск, ул. Студенческая 5
                    <br><br>+79089068257 
                    <br>hmaodiet@gmail.com 
                    <br>Пн-Вс с 10:00 до 21:00
                </h1>
            </div>
                <br>
                <div class="kartinki">
                <img class="sad" src="images/INSTA.png" width="62px" height="58px">
                <img class="sad" src="images/Facebook.png" width="62x" height="58px">
                <img class="bad" src="images/Twitter.png" width="62px" height="58px">
                </div>
        </div>
        </div>

    </div> 
    <?php require "footer.php";?>
</body>
</html>