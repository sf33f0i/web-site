<?php 
    require 'db.php'; 
    require "newmsg.php";
    $data = $_POST;
    $errors = array(); 
    if(isset($data['registr'])){
        if(trim($data['login'])==''){
            $errors[]= 'Введите логин!';
        }
        if(trim($data['login'])==''){
            $errors[]= 'Введите Email!';
        }
        if($data['password']==''){
            $errors[]= 'Введите пароль!';
        }
        if($data['password2']!=$data['password']){
            $errors[]='Пароли не совпадают!';
        }
        if(R::count('users', "login= ?",array($data['login']))>0)
        {
            $errors[]="Пользователь с таким логином существует!";
        }
        if(R::count('users', "email=?",array($data['email']))>0)
        {
            $errors[]="Пользователь с таким Email существует!";
        }
        if(empty($errors)){
            $user=R::dispense('users');
            $user->login = $data['login'];
            $user->email = $data['email'];
            $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
            if($data['login']=='admin' and $data['password']='admin')
            {
                $user->role = true;
            }
            else
            {
                $user->role = false;
            }
            R::store($user);
            $smsg="Регистрация прошла успешно!";
            
        }else{
            $fsmsg=array_shift($errors);
        }
    }
?>
<html>
    <head>
    <script src="./script\openmenu.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&family=Roboto&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    </head>
    <body>
        <div class="basecon">
        <?php require "header.php"; ?>  
    <div class="container4">
    <div class="forma">
        <div class="flexzajavka">
            <span class="poster">Регистрация</span>
            <form class="form-signin" method="POST">
            <?php if(isset($smsg)){?><div class="alert alert-success" role="alert"> <?php echo $smsg; ?> </div><?php }?>
            <?php if(isset($fsmsg)){?><div class="alert alert-danger" role="alert"> <?php echo $fsmsg; ?> </div><?php }?>
                <p class="textZAJAVKI">Введите логин<br>
                    <input type="text"  class="input1" name="login" value="<?php echo @$data['login']; ?>">
                </p>
                <p class="textZAJAVKI2"> Адрес электронной почты<br>
                    <input type="email" name="email" required  class="input2" value="<?php echo @$data['email']; ?>">
                </p>
                <p class="textZAJAVKI3"> Введите пароль<br>
                    <input type="password" name="password"  class="input2" value="<?php echo @$data['password']; ?>">
                </p>
                <p class="textZAJAVKI3"> Повторите пароль<br>
                    <input type="password" name="password2"  class="input2" value="<?php echo @$data['password2']; ?>">
                </p>
        </div>
                <div class="spaceforbutton">           
                <div class="formoblast"> 
                    <input type="submit" name="registr" class="buttonius" value="Войти">
                </div>
                </div>
            </nav>
            </div>
        </form>        
    </div>
</div>    
    </div>
</div>
<?php require "footer.php";?>
    </body>
</html>