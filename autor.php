<?php
    require 'db.php'; 
    require "newmsg.php";
    $data=$_POST;
    if(isset($data['autor'])){
        $errors= array();
       
        $user=R::findOne('users', 'login=?',array($data['login']));
        if( $user ){
            if(password_verify($data['password'], $user->password)){
                $_SESSION['logged_user']=$user;
                header('Location: index.php');
            }else{
                $errors[]='Неверно введен пароль!';
            }
        }else{
            $errors[]='Пользователь не найден';
        }
        if(!empty($errors)){
            $fsmsg=array_shift($errors);
        }
    }
        
        
    ?>
    
<html>
    <head>
    <script src="./script\openmenu.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&family=Roboto&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    </head>
    <body>
    <div class="basecon">
    <?php require "header.php"; ?>
    <div class="container4">
    <div class="forma">
        <div class="flexzajavka">
            <span class="poster">Вход</span>
            <form class="form-signin" method="POST">
            <?php if(isset($smsg)){?><div class="alert alert-success" role="alert"> <?php echo $smsg; ?> </div><?php }?>
            <?php if(isset($fsmsg)){?><div class="alert alert-danger" role="alert"> <?php echo $fsmsg; ?> </div><?php }?>
                <p class="textZAJAVKI">Логин<br>
                    <input type="text"  class="input1" name="login" value="<?php echo @$data['login'];?>">
                </p>
                <p class="textZAJAVKI3">Пароль<br>
                    <input type="password" name="password"  class="input2">
                </p>
        </div>
                <div class="spaceforbutton">           
                <div class="formoblast"> 
                    <input type="submit" name='autor' class="buttonius" value="Войти">
                </div>
                </div>
            </nav>
            </div>
        </form>        
    </div>
</div>    
    </div>
</div>
<?php require "footer.php";?>
    </body>
</html>