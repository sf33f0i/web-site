<?php 
require "db.php";
require "newmsg.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="./script\openmenu.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&family=Roboto&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dieta</title>
</head>
<body>
    <div class="basecon">
    <?php require "header.php"; ?>
    <div class="container">
        <div class="textb">
        <span> Консультации нутрициолога</span>
    </div> 
        <div class="text2">
            <span> ИНДИВИДУАЛЬНЫЙ ПОДХОД К ВАШЕМУ ПИТАНИЮ И ЗДОРОВЬЮ</span>
            
        </div>
        <div class="text3">
            
              <span>Нутрициолог занимается изучением вопросов, связанных с различными аспектами питания: состав продуктов питания, процесс приёма пищи, взаимодействие различных видов пищи, влияние пищи на организм.<br><br>
                В отличие от диетологии, нутрициология более комплексно подходит к изучению проблемы питания — от исследования мотивов выбора человеком тех или иных пищевых продуктов до механизмов внутриклеточного питания и их влияния на здоровье всего организма.</span>

        </div>
        <nav class="flexi">
            <div class="navigation2">
            <div id="onas">
            <a class="nav__link2" href="./onas.php">О нас</a>
        </div>
            </div>
            <div class="navigation3">
                <?php if(isset($_SESSION['logged_user'])): ?>
                <a class="nav__link3" href="./Zajavka.php">Оставить заявку</a>
                <?php else :?>
                    <a class="nav__link3" href="./autor.php">Оставить заявку</a>
                <?php endif ;?>
            </div>
        </nav>
        
        
</div>    
    </div>
    <div class="whiteBlock">
        <div class="whiteText1">
            <span>Наши компетенции</span>
        </div>
        <div class="whiteText2">
            <span>Наши специалисты имеют профильное образование и постоянно повышают квалификацию.</span>
        </div>
        <div class="whiteIMG">
            <img class="whiteIMG2" src="images/zxc.png">            
        </div>
    </div>
<?php require "footer.php";?>
</body>
</html>